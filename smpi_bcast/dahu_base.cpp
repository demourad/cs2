/* Copyright (c) 2010-2021. The SimGrid Team. All rights reserved.          */

/* This program is free software; you can redistribute it and/or modify it
 * under the terms of the license (GNU LGPL) which comes with this package. */

#include "simgrid/s4u.hpp"

namespace sg4 = simgrid::s4u;

/** @brief Create a simple platform based on Dahu cluster */
void create_dahu(const sg4::Engine& e)
{
  /**
   * Inspired on dahu cluster on Grenoble
   *     ________________
   *     |               |
   *     |     dahu      |
   *     |_______________|
   *     / /   | |    \ \
   *    / /    | |     \ \     <-- 12.5GBps links
   *   / /     | |      \ \
   * host1     ...      hostN
   */

  auto* root         = sg4::create_star_zone("dahu");
  std::string prefix = "dahu-", suffix = ".grid5000.fr";

  for (int id = 0; id < 32; id++) {
    std::string hostname = prefix + std::to_string(id) + suffix;
    /* create host */
    sg4::Host* host = root->create_host(hostname, 1)->set_core_count(32)->seal();
    /* create UP/DOWN link */
    sg4::Link* l_up   = root->create_link(hostname + "_up", 12.5e9)->set_latency(.1e-6)->seal();
    sg4::Link* l_down = root->create_link(hostname + "_down", 12.5e9)->set_latency(.1e-6)->seal();

    /* add link UP/DOWN for communications from the host */
    root->add_route(host->get_netpoint(), nullptr, nullptr, nullptr, std::vector<sg4::Link*>{l_up}, false);
    root->add_route(nullptr, host->get_netpoint(), nullptr, nullptr, std::vector<sg4::Link*>{l_down}, false);

    sg4::Link* loopback = root->create_link(hostname + "_loopback", 25e9)->set_latency(.1e-6)->seal();
    root->add_route(host->get_netpoint(), host->get_netpoint(), nullptr, nullptr, std::vector<sg4::Link*>{loopback});
  }

  root->seal();
}

