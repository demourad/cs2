/* Copyright (c) 2010-2021. The SimGrid Team. All rights reserved.          */

/* This program is free software; you can redistribute it and/or modify it
 * under the terms of the license (GNU LGPL) which comes with this package. */

#include "mpi.h"
#include "simgrid/s4u.hpp"

#include <unordered_set>

namespace sg4 = simgrid::s4u;

static void bcast_mpi(int argc, char* argv[])
{
  int retval;
  int msg_size = atoi(argv[0]);
  double* buf  = (double*)malloc(msg_size * sizeof(double));
  int root     = 0; // arbitrary choice
  int rank;

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_set_errhandler(MPI_COMM_WORLD, MPI_ERRORS_RETURN);

  retval = MPI_Bcast(buf, msg_size, MPI_DOUBLE, root, MPI_COMM_WORLD);
  if (retval != MPI_SUCCESS) {
    fprintf(stderr, "(%s:%d) MPI_Bcast() returned retval=%d\n", __FILE__, __LINE__, retval);
  }
  // printf("\t[%d] ok.\n", rank);
  MPI_Barrier(MPI_COMM_WORLD);
  free(buf);

  MPI_Finalize();
}

/*************************************************************************************************/

int main(int argc, char* argv[])
{
  if (argc < 5) {
    printf("Invalid platform.so params n_host_per_host msg_size bcast_type\n");
    return 0;
  }

  simgrid::s4u::Engine e(&argc, argv);
  smpi_init_options();

  e.set_config("smpi/host-speed:1f");
  e.set_config("smpi/simulate-computation:no");
  e.set_config("network/model:SMPI");
  if (std::string(argv[4]) == "ntls")
    e.set_config("smpi/bcast:NTSL");
  else if (std::string(argv[4]) == "smp_binomial")
    e.set_config("smpi/bcast:SMP_binomial");
  else if (std::string(argv[4]) == "smp_linear")
    e.set_config("smpi/bcast:SMP_linear");

  SMPI_init(); // should be called before configuring smpi/ options

  /* create platform */
  e.load_platform(argv[1]);

  int num_per_host = atoi(argv[2]);
  SMPI_app_instance_register("bcast_mpi", bcast_mpi, e.get_all_hosts().size() * num_per_host);
  int i = 0;
  for (auto* host : e.get_all_hosts()) {
    for (int j = 0; j < num_per_host; j++) {
      auto actor = sg4::Actor::create(std::string("bcast-") + std::to_string(j) + std::string(host->get_name()), host,
                                      "bcast_mpi", {argv[3]});
      actor->set_property("instance_id", "bcast_mpi");
      actor->set_property("rank", std::to_string(i));
      i++;
    }
  }

  e.run();

  printf("Simulation time %g\n", e.get_clock());

  SMPI_finalize();
  return 0;
}
