
PLATF="libdahu.so libdahuFactors.so"
STRAT="ntls smp_binomial smp_linear"
MSG="16000"
# MSG="100 16000 64000 128000 6000000"
NODES="1 8"
# NODES="1 8 16"

echo "platf,strat,msg_size,nodes,time"
for p in $PLATF; do
    for s in $STRAT; do
        for m in $MSG; do
            for n in $NODES; do
                FILENAME="bcast_${p}_${s}_${m}_${n}.trace"
                TIME=`./smpi_bcast --cfg=tracing:yes --cfg=tracing/filename:$FILENAME --cfg=tracing/smpi:yes --cfg=tracing/smpi/internals:yes ./$p $n $m $s 2>&1 | grep Simulation | cut -d" " -f3`;
                echo "$p,$s,$m,$n,$TIME"
            done
        done
    done
done
