/* Copyright (c) 2010-2021. The SimGrid Team. All rights reserved.          */

/* This program is free software; you can redistribute it and/or modify it
 * under the terms of the license (GNU LGPL) which comes with this package. */

#include "simgrid/kernel/resource/NetworkModelIntf.hpp"
#include "simgrid/s4u.hpp"
#include "dahu_base.hpp"

#include <unordered_set>

namespace sg4 = simgrid::s4u;

/*************************************************************************************************/
static const std::map<double, double> REMOTE_BW_FACTOR = {
    {0, 1.0000000000000002},         {8000, 1.0000000000000002},     {15798, 0.07435006650635523},
    {64000, 0.3163352696348148},     {6000000, 0.13003278960133288}, {42672591, 0.10354740223279707},
    {160097505, 0.40258935729656503}};
static const std::map<double, double> LOCAL_BW_FACTOR = {{0, 0.17591906192813994},
                                                         {16000, 0.12119203247138953},
                                                         {6000000, 0.07551057012803415},
                                                         {36900419, 0.04281516758309203},
                                                         {160097505, 0.17440518795992602}};

static const std::map<double, double> REMOTE_LAT_FACTOR = {{0, 0.0},
                                                           {8000, 1731.7102918851567},
                                                           {15798, 1441.073993161278},
                                                           {64000, 1761.4784830658123},
                                                           {6000000, 0.0},
                                                           {42672591, 0.0},
                                                           {160097505, 970913.4558162984}};
static const std::map<double, double> LOCAL_LAT_FACTOR  = {
    {0, 0.0}, {16000, 650.2212383180362}, {6000000, 0.0}, {36900419, 0.0}, {160097505, 1017885.3518765072}};

/** @brief Auxiliary method to get factor for a message size */
static double get_factor_from_map(const std::map<double, double>& factors, double size)
{
  double factor = 1.0;
  for (auto const& fact : factors) {
    if (size < fact.first) {
      break;
    } else {
      factor = fact.second;
    }
  }
  return factor;
}

/**
 * @brief Callback to set latency factor for a communication
 *
 * Set different factors for local (loopback) and remote communications.
 * Function signature is defined by API
 *
 * @param size Message size
 * @param src Host origin
 * @param dst Host destination
 */
static double latency_factor_cb(double size, const sg4::Host* src, const sg4::Host* dst,
                                const std::vector<sg4::Link*>& /*links*/,
                                const std::unordered_set<sg4::NetZone*>& /*netzones*/)
{
  if (src->get_name() == dst->get_name()) {
    /* local communication factors */
    return get_factor_from_map(LOCAL_LAT_FACTOR, size);
  } else {
    return get_factor_from_map(REMOTE_LAT_FACTOR, size);
  }
}

/**
 * @brief Callback to set bandwidth factor for a communication
 *
 * Set different factors for local (loopback) and remote communications.
 * Function signature is defined by API
 *
 * @param size Message size
 * @param src Host origin
 * @param dst Host destination
 */
static double bandwidth_factor_cb(double size, const sg4::Host* src, const sg4::Host* dst,
                                  const std::vector<sg4::Link*>& /*links*/,
                                  const std::unordered_set<sg4::NetZone*>& /*netzones*/)
{
  if (src->get_name() == dst->get_name()) {
    /* local communication factors */
    return get_factor_from_map(LOCAL_BW_FACTOR, size);
  } else {
    return get_factor_from_map(REMOTE_BW_FACTOR, size);
  }
}

extern "C" void load_platform(const sg4::Engine& e);
void load_platform(const sg4::Engine& e)
{
  create_dahu(e);
  simgrid::kernel::resource::NetworkModelIntf* model = e.get_netzone_root()->get_network_model();
  model->set_lat_factor_cb(&latency_factor_cb);
  model->set_bw_factor_cb(bandwidth_factor_cb);
}