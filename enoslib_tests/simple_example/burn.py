import logging
import asyncio
import aiocoap
import argparse

import random
import time

from itertools import starmap
from operator import mul

logging.basicConfig(level=logging.INFO)

def burn_cpu(N):
    old = time.time()
    a = [ [random.random() for i in range(0,N)] for j in range(0,N)]
    b = [ [random.random() for i in range(0,N)] for j in range(0,N)]
    # avoid using numpy since A8 nodes doesn't have it installed for python 3.5
    [[sum(starmap(mul, zip(row, col))) for col in zip(*b)] for row in a]
    elapsed = time.time() - old
    return elapsed

def observation_callback(response, N):
    elapsed = burn_cpu(N)
    print("Message received: %r, elapsed_time: %f" % (response.payload, elapsed))


async def read_sensor(protocol, sensor_addr, N, port = 5683):
    request = aiocoap.Message(code=aiocoap.GET, uri='coap://[%s]:%d/sensors/gyros' % (sensor_addr, port))

    try:
        response = await protocol.request(request).response
    except Exception as e:
        print('Failed to fetch resource:')
        print(e)
    else:
        elapsed = burn_cpu(N)
        print('Result: %r, elapsed: %f'% (response.payload, elapsed))

async def main(args):
    protocol = await aiocoap.Context.create_client_context()

    request = aiocoap.Message(code=aiocoap.GET, uri='coap://[%s]:%d/test/push' % (args.sensor, 5683))
    request.opt.observe = 0
    protocol_request = protocol.request(request)
    protocol_request.observation.register_callback(lambda response: observation_callback(response, args.matrix*5))

    while (True):
        await read_sensor(protocol, args.sensor, args.matrix)
        await asyncio.sleep(1)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('--sensor', '-s', type=str,
                    help='sensor IPv6 address')
    parser.add_argument('--matrix', '-m', type=int,
                    help='Matrix size')
    args = parser.parse_args()
    random.seed(42)

    asyncio.get_event_loop().run_until_complete(main(args))
